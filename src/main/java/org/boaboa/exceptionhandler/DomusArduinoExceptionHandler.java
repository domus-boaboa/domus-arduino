package org.boaboa.exceptionhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import jssc.SerialPortException;

@ControllerAdvice
public class DomusArduinoExceptionHandler {
	
	private static Logger LOGGER = LoggerFactory.getLogger(DomusArduinoExceptionHandler.class);
	
	@ExceptionHandler(SerialPortException.class)
	public void serialPortException(SerialPortException e) {
		LOGGER.error("Fallo la conexión con arduino!" + e.getPortName() + " método " + e.getMethodName());
	}

}

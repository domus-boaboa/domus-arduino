package org.boaboa.service;

import org.boaboa.arduino.NotParse;

import jssc.SerialPortException;

public interface ArduinoService {

	Object run(String pin, String command, Boolean status) throws SerialPortException, NotParse;

}

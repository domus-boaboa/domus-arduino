package org.boaboa.service;

import org.boaboa.arduino.ArduinoSerial;
import org.boaboa.arduino.NotParse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jssc.SerialPortException;

@Service
public class ArduinoServiceImpl implements ArduinoService {

	@Autowired
	private ArduinoSerial _arduino;

	@Override
	public Object run(String pin, String command, Boolean status) throws SerialPortException, NotParse {
		String val = status ? "1" : "0";
		_arduino.setPin(pin);
		_arduino.setVal(val);
		if (command.equals("DW")) {
			_arduino.setCommand("1");
		} else {
			_arduino.setCommand("4");
		}
		return _arduino.runCommand();
	}

}

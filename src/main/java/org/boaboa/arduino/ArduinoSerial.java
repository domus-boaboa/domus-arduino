package org.boaboa.arduino;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jssc.SerialPort;
import jssc.SerialPortException;

@Component
public class ArduinoSerial {

	private static Logger LOGGER = LoggerFactory.getLogger(ArduinoSerial.class);

	@Autowired
	private SerialPort ino;

	private String pin;

	private String val;

	private String command;

	public static final String EXCLAMATION = "!";

	public static final String POINT = ".";

	public static final String SPACE = " ";

	public Object runCommand() throws SerialPortException, NotParse {
		String commandDuino = generateCommand();
		LOGGER.info("Arduino Send message: [" + commandDuino + "]");
		Object outPut = null;
		ino.writeString(commandDuino);
		if (command.equals("4")) {
			outPut = ArduinoUtils.integerOutPutArduino(ino.readString(9));
			LOGGER.info("Recived arduino " + outPut );
		}
		return outPut;
	}

	private String generateCommand() {
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(EXCLAMATION);
		strBuffer.append(command);
		strBuffer.append(SPACE);
		strBuffer.append(pin);
		if (!StringUtils.isEmpty(val)) {
			strBuffer.append(SPACE);
			strBuffer.append(val);
		}
		strBuffer.append(POINT);
		return strBuffer.toString();
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}

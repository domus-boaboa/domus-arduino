package org.boaboa.arduino;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArduinoUtils {

	public static final Pattern splitter = Pattern.compile("\\d+");

	public static int integerOutPutArduino(String message) throws NotParse {
		String substring = message.substring(4, message.length());
		Matcher m = splitter.matcher(substring);
		while (m.find()) {
			int n = Integer.parseInt(m.group());
			return n;
		}
		throw new NotParse();
	}

}

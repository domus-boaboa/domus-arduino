package org.boaboa.controller;

import java.util.HashMap;
import java.util.Map;

import org.boaboa.arduino.NotParse;
import org.boaboa.service.ArduinoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jssc.SerialPortException;

@Controller
@RequestMapping("/arduino")
public class ArduinoController {

	@Autowired
	private ArduinoService _arduinoService;

	@RequestMapping(path = "{pin}/{command}/{status}", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> acctionArduino(@PathVariable String pin, @PathVariable String command,
			@PathVariable Boolean status) throws SerialPortException, NotParse {
		Object result = _arduinoService.run(pin,command,status);
		Map<String, Object> map = new HashMap<>();
		map.put("result", result);
		return new ResponseEntity<Map<String,Object>>(map, HttpStatus.OK);
	}

}

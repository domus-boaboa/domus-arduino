package org.boaboa.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jssc.SerialPort;
import jssc.SerialPortException;

@Configuration
public class ArduinoConfiguration {
	
	@Value("${arduino.port}")
	private String port;

	@Bean
	public SerialPort arduinTXRX() throws SerialPortException {
		SerialPort ino = new SerialPort(port);
		ino.openPort();
		ino.setParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
		return ino;
	}
}

package org.boaboa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomusArduinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomusArduinoApplication.class, args);
	}
}

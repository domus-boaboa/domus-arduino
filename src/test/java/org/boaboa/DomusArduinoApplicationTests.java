package org.boaboa;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.boaboa.arduino.ArduinoUtils;
import org.boaboa.arduino.NotParse;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DomusArduinoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void parseIntOutputArduino() throws NotParse {
		String outPut = "A0::393\\r\\n";
		int integerOutPutArduino = ArduinoUtils.integerOutPutArduino(outPut);
		assertNotNull(integerOutPutArduino);
		assertTrue(integerOutPutArduino == 393);
	}

	@Test
	public void exceptionIntOutputArduino() {
		String outPut = "A0::asda\\r\\n";
		try {
			ArduinoUtils.integerOutPutArduino(outPut);
		} catch (NotParse e) {

		}
	}

}
